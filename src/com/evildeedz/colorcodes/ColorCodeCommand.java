package com.evildeedz.colorcodes;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class ColorCodeCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			player.sendMessage(ChatColor.YELLOW.toString() + ChatColor.BOLD + "======" + ChatColor.DARK_PURPLE + " ColorCodes v1.0 by: " + ChatColor.RED.toString() + ChatColor.BOLD + "ALEviLDeEdZ " + ChatColor.YELLOW.toString() + ChatColor.BOLD + "======" );
			player.sendMessage(" ");
			player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "Color Codes:");
			player.sendMessage(ChatColor.WHITE + "&1 " + ChatColor.DARK_GRAY + " >> " + ChatColor.BLUE + " Blue");
			player.sendMessage(ChatColor.WHITE + "&2 " + ChatColor.DARK_GRAY + " >> " + ChatColor.DARK_GREEN + " Dark Green");
			player.sendMessage(ChatColor.WHITE + "&3 " + ChatColor.DARK_GRAY + " >> " + ChatColor.DARK_AQUA + " Dark Aqua");
			player.sendMessage(ChatColor.WHITE + "&4 " + ChatColor.DARK_GRAY + " >> " + ChatColor.DARK_RED + " Dark Red");
			player.sendMessage(ChatColor.WHITE + "&5 " + ChatColor.DARK_GRAY + " >> " + ChatColor.DARK_PURPLE + " Dark Purple");
			player.sendMessage(ChatColor.WHITE + "&6 " + ChatColor.DARK_GRAY + " >> " + ChatColor.GOLD + " Gold");
			player.sendMessage(ChatColor.WHITE + "&7 " + ChatColor.DARK_GRAY + " >> " + ChatColor.GRAY + " Gray");
			player.sendMessage(ChatColor.WHITE + "&8 " + ChatColor.DARK_GRAY + " >> " + ChatColor.DARK_GRAY + " Dark Gray");
			player.sendMessage(ChatColor.WHITE + "&9 " + ChatColor.DARK_GRAY + " >> " + ChatColor.DARK_BLUE + " Dark Blue");
			player.sendMessage(ChatColor.WHITE + "&0 " + ChatColor.DARK_GRAY + " >> " + ChatColor.BLACK + " Black");
			player.sendMessage(ChatColor.WHITE + "&a " + ChatColor.DARK_GRAY + " >> " + ChatColor.GREEN + " Green");
			player.sendMessage(ChatColor.WHITE + "&b " + ChatColor.DARK_GRAY + " >> " + ChatColor.AQUA + " Aqua");
			player.sendMessage(ChatColor.WHITE + "&c " + ChatColor.DARK_GRAY + " >> " + ChatColor.RED + " Red");
			player.sendMessage(ChatColor.WHITE + "&d " + ChatColor.DARK_GRAY + " >> " + ChatColor.LIGHT_PURPLE + " Light Purple");
			player.sendMessage(ChatColor.WHITE + "&e " + ChatColor.DARK_GRAY + " >> " + ChatColor.YELLOW + " Yellow");
			player.sendMessage(ChatColor.WHITE + "&f " + ChatColor.DARK_GRAY + " >> " + ChatColor.WHITE + " White");
			player.sendMessage(" ");
			player.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "Formatting:");
			player.sendMessage(ChatColor.WHITE + "&k " + ChatColor.DARK_GRAY + " >> " + ChatColor.WHITE.toString() + ChatColor.MAGIC + " Magic");
			player.sendMessage(ChatColor.WHITE + "&l " + ChatColor.DARK_GRAY + " >> " + ChatColor.WHITE.toString() + ChatColor.BOLD + " Bold");
			player.sendMessage(ChatColor.WHITE + "&m " + ChatColor.DARK_GRAY + " >> " + ChatColor.WHITE.toString() + ChatColor.STRIKETHROUGH + " Strikethrough");
			player.sendMessage(ChatColor.WHITE + "&n " + ChatColor.DARK_GRAY + " >> " + ChatColor.WHITE.toString() + ChatColor.UNDERLINE + " Underline");
			player.sendMessage(ChatColor.WHITE + "&o " + ChatColor.DARK_GRAY + " >> " + ChatColor.WHITE.toString() + ChatColor.ITALIC + " Italic");
			
		}
		else
		{
			System.out.println("Command cant be ran from console");
		}
		return false;
	}
}
