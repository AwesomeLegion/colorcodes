package com.evildeedz.colorcodes;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	@Override
	public void onEnable()
	{
		System.out.println("ColorCodes v1.0 Started, Author: ALEviLDeEdZ");
		getCommand("colorcodes").setExecutor(new ColorCodeCommand());
	}
}
